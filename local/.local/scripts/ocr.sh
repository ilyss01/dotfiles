#!/usr/bin/env sh

TEMP=$(mktemp -d)

grimblast copysave area $TEMP/img.png;
mogrify -modulate 100,0 -resize 400% $TEMP/img.png;
tesseract $TEMP/img.png $TEMP/text &> /dev/null;
wl-copy < $TEMP/text.txt;
notify-send "OCR done"
