#!/usr/bin/env sh
# Choosing best mirror for pacman

sudo reflector --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syyu
