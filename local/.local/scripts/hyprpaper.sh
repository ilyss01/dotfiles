#!/usr/bin/env sh

# hack for hyprland not being able to set a new wallpaper that is not loaded into memory
sleep 0.5

apply_wallpapers() {
	directory=~/pictures/wallpapers
	monitors=`hyprctl monitors | grep Monitor | awk '{print $2}'`
	random_background=$(ls $directory | shuf -n 1)
	hyprctl hyprpaper preload $directory/$random_background
	for monitor in $monitors
	do
		hyprctl hyprpaper wallpaper "$monitor, $directory/$random_background"
	done
}

handle() {
	case $1 in
		monitoradded*)
			apply_wallpapers
			hyprctl hyprpaper unload unused
			;;
	esac
}

# initial wallpaper
apply_wallpapers

# reapply random wallpaper as soon as there is a new monitor
socat -U - UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock | while read -r line; do handle "$line"; done
