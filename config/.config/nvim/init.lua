local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
local bind = vim.keymap.set
local vimo = vim.opt
local vimg = vim.g
local vimcmd = vim.cmd

-- lazy.nvim setup
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system {
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable",
        lazypath,
    }
end
vimo.rtp:prepend(lazypath)

-- Leader remap
vimg.mapleader = " "
vimg.maplocalleader = " "

-- Show tabs
vimo.list = true

-- Shared system clipboard
-- vimo.clipboard = "unnamedplus"

-- No vi compatibility
vimo.compatible = false

-- Speed
vimo.ttyfast = true
vimo.lazyredraw = true

-- Syntax
vimo.syntax = "on"

-- Break indent
vimo.breakindent = true

-- Numbers
vimo.number = true

-- Amount of lines below cursor
vimo.scrolloff = 15

-- Normal search
vimo.hlsearch = true
vimo.ignorecase = true
vimo.smartcase = true
vimo.wrapscan = true

-- Sane scroll
vimo.wrap = true
vimo.smoothscroll = true

-- Folding
vimo.foldmethod = "indent"
vimo.foldlevelstart = 20

-- Sane bar
vimo.cmdheight = 0
vimo.laststatus = 0

-- Splits
vimo.splitbelow = true
vimo.splitright = true

-- Color fix for tmux
vimo.termguicolors = true

-- Plugins
require("lazy").setup({

    -- Colorscheme
    {
        "Tsuzat/NeoSolarized.nvim",
        lazy = false,
        priority = 1000,
        config = function()
            local neosolarized = require("NeoSolarized")
            neosolarized.setup {
                style = "dark",
                transparent = false,
                terminal_colors = false,
                enable_italics = false,
                styles = {
                    comments = { italic = false },
                    keywords = { italic = false },
                    functions = { bold = false },
                    string = { italic = false },
                    underline = false,
                    undercurl = false,
                },
            }
            vimcmd.colorscheme("NeoSolarized")
        end,
    },

    -- Detect tabstop and shiftwidth automatically
    "tpope/vim-sleuth",

    -- Better highlighting
    {
        "nvim-treesitter/nvim-treesitter",
        version = false,
        build = ":TSUpdate",
        event = { "BufReadPost", "BufNewFile" },
        opts = {
            ensure_installed = {
                "lua", "c", "cpp", "python", "rust", "markdown", "toml", "haskell", "gitcommit",
                "vimdoc", "ini", "json", "make", "latex"
            },
            ignore_install = {
                "javascript", "node", "typescript"
            },
            indent = { enable = true },
            sync_install = false,
            auto_install = true,
            highlight = {
                enable = true,
                additional_vim_regex_highlighting = { "markdown" },
            },
        },
        config = function(opts)
            require("nvim-treesitter.configs").setup(opts)
        end,
    },

    -- LSP
    {
        "neovim/nvim-lspconfig",
        config = function()
            local lspconfig = require("lspconfig")
            lspconfig.clangd.setup({})
            lspconfig.gopls.setup({})
            --lspconfig.pylsp.setup({})
            lspconfig.pyright.setup({})
            lspconfig.rust_analyzer.setup({})
            lspconfig.lua_ls.setup({})
            lspconfig.hls.setup({})
            lspconfig.marksman.setup({})
            lspconfig.texlab.setup({})
            lspconfig.bashls.setup({})
        end,
    },

    -- Completion
    {
        "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "neovim/nvim-lspconfig"
        },
        config = function()
            local capabilities = require("cmp_nvim_lsp").default_capabilities()
            local lspconfig = require('lspconfig')
            local cmp = require 'cmp'

            local servers = {
                'clangd', 'rust_analyzer', 'pyright', 'gopls', 'lua_ls', 'hls', 'marksman', 'texlab', 'bashls'
            }
            for _, lsp in ipairs(servers) do
                lspconfig[lsp].setup {
                    capabilities = capabilities,
                }
            end
            cmp.setup {
                mapping = cmp.mapping.preset.insert({
                    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
                    ['<C-d>'] = cmp.mapping.scroll_docs(4),
                    ['<C-Space>'] = cmp.mapping.complete(),
                    ['<CR>'] = cmp.mapping.confirm {
                        behavior = cmp.ConfirmBehavior.Replace,
                        select = true,
                    },
                    ['<Tab>'] = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_next_item()
                        else
                            fallback()
                        end
                    end, { 'i', 's' }),
                    ['<S-Tab>'] = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_prev_item()
                        else
                            fallback()
                        end
                    end, { 'i', 's' }),
                }),
                sources = {
                    { name = 'nvim_lsp' }
                },
            }
        end,
    },

}, {})

-- ESC in search disables highlight
bind("n", "<ESC>", "<cmd>nohlsearch<CR>")

-- LaTeX
bind("n", "<leader>lc", ":silent !latexmk -pdf -output-directory=build %<CR>") -- Compile LaTeX document
bind("n", "<leader>lp", "<cmd>silent !setsid -f zathura build/%:r.pdf<CR>")    -- Show compiled document"

-- Movement fix
bind("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
bind("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Helix binds
bind({ "n", "v" }, "ge", "G")
bind("n", "U", "<C-r>")
bind("n", "<C-r>", "")

-- System clipboard
bind({"n", "v"}, "<leader>y", "\"+y")
bind({"n", "v"}, "<leader>Y", "\"+y")
bind({"n", "v"}, "<leader>p", "\"+p")
bind({"n", "v"}, "<leader>P", "\"+p")

-- Tabs
bind("n", "<C-t>o", vimcmd.tabnew)
bind("n", "<C-t>n", vimcmd.tabnext)
bind("n", "<C-t>p", vimcmd.tabprevious)
bind("n", "<C-t>N", vimcmd.tabprevious)
bind("n", "<C-t>q", vimcmd.tabclose)

-- LSP
bind('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>')
bind('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>')
bind('n', 'gtd', '<cmd>lua vim.lsp.buf.type_definition()<cr>')
bind('n', '<leader>r', '<cmd>lua vim.lsp.buf.rename()<cr>')
bind('n', '<leader>a', '<cmd>lua vim.lsp.buf.code_action()<cr>')
bind('n', '<leader>h', '<cmd>lua vim.lsp.buf.hover()<cr>')
bind('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>')
bind('n', '<F3>', '<cmd>lua vim.lsp.buf.format()<cr>')

-- Diagnostics
bind('n', '<C-[>', '<cmd>lua vim.diagnostic.goto_prev()<cr>')
bind('n', '<C-]>', '<cmd>lua vim.diagnostic.goto_next()<cr>')

-- Filemanager
bind("n", "<leader>f", "<cmd>Explore<cr>")

vimg.netrw_banner = 0
vimg.netrw_liststyle = 3

vim.api.nvim_create_autocmd("filetype", {
    pattern = "netrw",
    desc = "Better mappings for netrw",
    callback = function()
        local netrw_bind = function(lhs, rhs)
            bind("n", lhs, rhs, { remap = true, buffer = true })
        end
        -- New file
        netrw_bind("i", "%")
        netrw_bind("I", "%")
        netrw_bind("a", "%")
        netrw_bind("A", "%")

        -- New directory
        netrw_bind("o", "d")
        netrw_bind("O", "d")

        -- Rename
        netrw_bind("r", "R")

        -- Remove
        netrw_bind("dd", "D")
    end
})
