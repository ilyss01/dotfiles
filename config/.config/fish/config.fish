# Default programs
set -gx PAGER "less"
set -gx EDITOR "nvim"
set -gx VISUAL "nvim"
set -gx BROWSER "firefox"

# XDG ~/ Clean-up
set -gx XDG_CONFIG_HOME "$HOME/.config"
set -gx XDG_DATA_HOME "$HOME/.local/share"
set -gx XDG_CACHE_HOME "$HOME/.cache"
set -gx XDG_STATE_HOME "$HOME/.local/state"
set -gx XDG_DESKTOP_DIR "$HOME/"
set -gx XDG_DOWNLOAD_DIR "$HOME/downloads"
set -gx XDG_TEMPLATES_DIR "$HOME/"
set -gx XDG_PUBLICSHARE_DIR "$HOME/downloads"
set -gx XDG_DOCUMENTS_DIR "$HOME/documents"
set -gx XDG_MUSIC_DIR "$HOME/music"
set -gx XDG_PICTURES_DIR "$HOME/pictures"
set -gx XDG_VIDEOS_DIR "$HOME/"

# ~/ Clean-up
set -gx RUSTUP_HOME "$XDG_DATA_HOME/rustup"
set -gx CARGO_HOME "$XDG_DATA_HOME/cargo"
set -gx GOPATH "$XDG_DATA_HOME/go"
set -gx GOMODCACHE "$XDG_CACHE_HOME/go/mod"
set -gx PYTHONPYCACHEPREFIX "$XDG_CACHE_HOME/python"
set -gx PYTHONUSERBASE "$XDG_DATA_HOME/python"
set -gx PYTHON_HISTORY "$XDG_STATE_HOME/python/history"
set -gx JUPYTER_PLATFORM_DIRS "$XDG_CONFIG_HOME/jupyter"
set -gx TEXMFHOME "$XDG_DATA_HOME/texmf"
set -gx TEXMFVAR "$XDG_CACHE_HOME/texlive/texmf-var"
set -gx TEXMFCONFIG "$XDG_CONFIG_HOME/texlive/texmf-config"
set -gx CABAL_CONFIG "$XDG_CACHE_HOME/cabal"
set -gx GHCUP_USE_XDG_DIRS true
set -gx GRADLE_USER_HOME "$XDG_DATA_HOME/gradle"
# set -gx _JAVA_OPTIONS=-Djava.util.prefs.userRoot "$XDG_CONFIG_HOME/java"
# set -gx _JAVA_OPTIONS=-Djava.util.prefs.userRoot "$XDG_CONFIG_HOME/java"
set -gx LEIN_HOME "$XDG_DATA_HOME/lein"
set -gx ANDROID_SDK_HOME "$XDG_CONFIG_HOME/android"
set -gx WINEPREFIX "$XDG_DATA_HOME/wineprefixes/default"
set -gx HISTFILE "$XDG_DATA_HOME/zsh/history"
set -gx PASSWORD_STORE_DIR "$XDG_DATA_HOME/pass"
set -gx SQLITE_HISTORY "$XDG_DATA_HOME/sqlite_history"
set -gx GNUPGHOME "$XDG_DATA_HOME/gnupg"
set -gx DOCKER_CONFIG "$XDG_CONFIG_HOME/docker"
set -gx MACHINE_STORAGE_PATH "$XDG_DATA_HOME/docker-machine"
set -gx JULIA_DEPOT_PATH "$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH"
set -gx JULIAUP_DEPOT_PATH "$XDG_DATA_HOME/julia"
set -gx PARALLEL_HOME "$XDG_CONFIG_HOME/parallel"

# Graphics stuff
set -gx LIBVA_DRIVER_NAME "iHD"
set -gx VDPAU_DRIVER "va_gl"

if status is-interactive
    set fish_greeting
    function fish_user_key_bindings
        fish_default_key_bindings -M insert
        fish_vi_key_bindings --no-erase insert
    end

    set fish_cursor_default block
    set fish_cursor_insert line
    set fish_cursor_replace_one underscore
    set fish_cursor_visual block

    abbr -a mkdir "mkdir -p"
    abbr -a cp "cp -r"
    abbr -a cdc "cd ~/.config/"
    abbr -a cdd "cd ~/.dotfiles/"
    abbr -a ka "killall"

    # Package manager binds
    abbr -a pS "sudo pacman -S"
    abbr -a pSs "pacman -Ss"
    abbr -a pR "sudo pacman -R"
    abbr -a pSyu "sudo pacman -Syu"
    abbr -a pOrphan "sudo pacman -Qtdq | sudo pacman -Rns -"

    abbr -a n "nvim"
    abbr -a upd "sudo pacman -Syu; ~/.local/scripts/fwupd.sh"
    abbr -a latexmk "latexmk -pdf -outdir=build/"

    function yma
        yt-dlp --cookies-from-browser firefox -N 15 --embed-metadata $argv

        # It kinda works better? Can't measure it but would belive that it's true
        # yt-dlp --cookies-from-browser firefox -g $argv > $XDG_CACHE_HOME/yt-dlp-temp
        # parallel --line-buffer --jobs 8 -a $XDG_CACHE_HOME/yt-dlp-temp yt-dlp --cookies-from-browser firefox $argv
    end

    function lfcd --wraps="lf"
        cd "$(command lf -print-last-dir $argv)"
    end

    abbr -a lf "lfcd"

    zoxide init fish | source
end
